Sentiment Monitor Test
======================
Sentiment Monitor Test adalah aplikasi untuk menguji modul klasifikasi dari aplikasi [Sentiment Monitor](https://gitlab.com/RadhiFadlillah/sentiment-monitor).

Cara Installasi
---------------
1. Download rilis terbaru dari halaman [_release_](https://gitlab.com/RadhiFadlillah/sentiment-monitor-test/tags) sesuai dengan platform yang digunakan.
2. Ekstrak hasil download ke folder yang diinginkan.
3. Di folder tersebut, buat folder baru dengan nama `db`.
4. Download semua [database](https://gitlab.com/RadhiFadlillah/sentiment-monitor-test/tree/master/db), lalu letakkan di folder `db` yang telah dibuat sehingga struktur direktorinya menjadi sebagai berikut :

    ```
    sentiment-monitor-test*
    db/
    ├── testing-set.db
    ├── training-set-medium.db
    ├── training-set-small.db
    └── training-set-smallest.db
    ```

5. Aplikasi `sentiment-monitor-test` siap digunakan.

Cara Penggunaan Aplikasi
------------------------
1. Jalankan aplikasi `sentiment-monitor-test`. Jika di Linux, jalankan lewat `terminal`, jika di Windows bisa langsung klik dua kali.
2. Pilih metode pengujian.
3. Tunggu pengujian dilaksanakan.
4. Lihat hasil pengujian.

Lisensi
---------
*Source code* aplikasi ini disebar di bawah lisensi [GPLv3](http://choosealicense.com/licenses/gpl-3.0/) sehingga setiap orang dapat menggunakan, memodifikasi dan menyebarkannya, dengan syarat *souce code* tetap dibuka dan tetap dirilis di bawah lisensi GPLv3.

```
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
```