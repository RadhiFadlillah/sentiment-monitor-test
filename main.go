package main

import (
	"database/sql"
	"fmt"
	"math"
	"os"

	"github.com/RadhiFadlillah/go-bayesian"
	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
	"github.com/olekukonko/tablewriter"
	"gopkg.in/cheggaaa/pb.v1"
)

const (
	testingSetDB          = "./db/testing-set.db"
	trainingSetMediumDB   = "./db/training-set-medium.db"
	trainingSetSmallDB    = "./db/training-set-small.db"
	trainingSetSmallestDB = "./db/training-set-smallest.db"
)

func main() {
	var testingMethod int
	fmt.Println("Testing for classification module in sentiment monitor :")
	fmt.Println("1. Using one set testing data against twenty set training data.")
	fmt.Println("2. Using k-fold cross validation.")
	fmt.Println("")
	fmt.Print("Choose testing method [1/2]: ")
	fmt.Scanln(&testingMethod)

	if testingMethod != 1 && testingMethod != 2 {
		fmt.Println("Error: Choose between 1 and 2.")
		return
	}

	if testingMethod == 1 {
		testMethod1()
		return
	}

	var datasetID int
	var k int
	fmt.Println("")
	fmt.Println("Do k-fold cross validation testing using :")
	fmt.Println("1.   600,000 data; 200,000 for each sentiment.")
	fmt.Println("2.   900,000 data; 300,000 for each sentiment.")
	fmt.Println("3. 1,200,000 data; 400,000 for each sentiment.")
	fmt.Println("")
	fmt.Print("Choose dataset [1/2/3]: ")
	fmt.Scanln(&datasetID)
	fmt.Print("Input k-size          : ")
	fmt.Scanln(&k)

	if datasetID < 1 || datasetID > 3 {
		fmt.Println("Error: Choose between 1, 2 and 3.")
		return
	}

	if k < 2 {
		fmt.Println("Error: k must be equal or bigger than 2.")
		return
	}

	if datasetID == 1 {
		testMethod2(trainingSetSmallestDB, k)
	} else if datasetID == 2 {
		testMethod2(trainingSetSmallDB, k)
	} else {
		testMethod2(trainingSetMediumDB, k)
	}
}

func testMethod1() {
	// Connect to database
	trainingDB := sqlx.MustConnect("sqlite3", trainingSetSmallDB)
	testingDB := sqlx.MustConnect("sqlite3", testingSetDB)
	defer func() {
		trainingDB.Close()
		testingDB.Close()
	}()

	// Prepare query for selecting training set
	selectTraining, err := trainingDB.Preparex(`
		SELECT * FROM (SELECT * FROM training_set WHERE sentiment = 1 ORDER BY RANDOM() LIMIT ?) UNION 
		SELECT * FROM (SELECT * FROM training_set WHERE sentiment = 0 ORDER BY RANDOM() LIMIT ?) UNION 
		SELECT * FROM (SELECT * FROM training_set WHERE sentiment = -1 ORDER BY RANDOM() LIMIT ?)`)
	checkError(err)

	// Select testing set from database
	testingSet := []Tweet{}
	err = testingDB.Select(&testingSet, "SELECT * FROM training_set")
	checkError(err)

	// Create test result
	testResult := []TestResult{}

	// Create cleaner
	cleaner := NewCleaner()

	// Define bayesian class
	classPositive := bayesian.Class("positive")
	classNegative := bayesian.Class("negative")
	classNeutral := bayesian.Class("neutral")

	// Begin process
	for i := 1; i <= 20; i++ {
		// Show message
		fmt.Println("")
		fmt.Println("Iteration", i)

		// Fetch training set
		limit := 15000 * i
		trainingSet := []Tweet{}
		err := selectTraining.Select(&trainingSet, limit, limit, limit)
		checkError(err)

		// Create classifier
		classifier := bayesian.NewClassifier(bayesian.MultinomialBoolean)

		// Create progress bar for training
		progressTraining := pb.StartNew(len(trainingSet)).Prefix("Training")

		// Begin training
		for _, doc := range trainingSet {
			// Clean document
			words := cleaner.Clean(doc.Tweet)

			// If there are no words, skip
			if len(words) == 0 {
				continue
			}

			// Check document class
			var docClass bayesian.Class
			if doc.Sentiment == 1 {
				docClass = classPositive
			} else if doc.Sentiment == 0 {
				docClass = classNeutral
			} else {
				docClass = classNegative
			}

			// Do learning for this doc
			learningDoc := bayesian.NewDocument(docClass, words...)
			classifier.Learn(learningDoc)

			// Increment progress bar
			progressTraining.Increment()
		}

		// Finish progress bar for training
		progressTraining.Finish()

		// Create testing result
		result := TestResult{
			Iteration: i,
			NData:     limit,
		}

		// Create progress bar for testing
		progressTesting := pb.StartNew(len(testingSet)).Prefix("Testing")

		// Begin testing
		for _, doc := range testingSet {
			// Clean document
			words := cleaner.Clean(doc.Tweet)

			// Classify document
			_, class, _ := classifier.Classify(words...)

			// Save result
			if doc.Sentiment == 1 && class == classPositive {
				result.NPositive++
			} else if doc.Sentiment == 0 && class == classNeutral {
				result.NNeutral++
			} else if doc.Sentiment == -1 && class == classNegative {
				result.NNegative++
			}

			// Increment progress bar
			progressTesting.Increment()
		}

		// Finish progress bar for testing
		progressTesting.Finish()

		// Push iteration result to main testing result
		testResult = append(testResult, result)
	}

	// Create table
	fmt.Println("")
	fmt.Println("Final result")
	table := tablewriter.NewWriter(os.Stdout)

	// Write header
	table.SetHeader([]string{
		"Iteration",
		"Training Size",
		"% Positive",
		"% Negative",
		"% Neutral",
		"% Total",
	})

	// Write content
	for _, result := range testResult {
		totalResult := result.NPositive + result.NNegative + result.NNeutral
		trainingSize := result.NData * 3
		percentPositive := float64(result.NPositive) / float64(100000) * 100.0
		percentNegative := float64(result.NNegative) / float64(100000) * 100.0
		percentNeutral := float64(result.NNeutral) / float64(100000) * 100.0
		percentTotal := float64(totalResult) / float64(300000) * 100.0

		table.Append([]string{
			fmt.Sprintf("%d", result.Iteration),
			fmt.Sprintf("%d", trainingSize),
			fmt.Sprintf("%.2f", percentPositive),
			fmt.Sprintf("%.2f", percentNegative),
			fmt.Sprintf("%.2f", percentNeutral),
			fmt.Sprintf("%.2f", percentTotal),
		})
	}

	// Render table
	table.Render()
}

func testMethod2(dbPath string, k int) {
	// Connect to database
	db := sqlx.MustConnect("sqlite3", dbPath)
	defer db.Close()

	// Prepare query for selecting training set
	selectTesting, err := db.Preparex(`
		SELECT * FROM (SELECT * FROM training_set WHERE sentiment = 1 ORDER BY id LIMIT ? OFFSET ?) UNION
		SELECT * FROM (SELECT * FROM training_set WHERE sentiment = 0 ORDER BY id LIMIT ? OFFSET ?) UNION
		SELECT * FROM (SELECT * FROM training_set WHERE sentiment = -1 ORDER BY id LIMIT ? OFFSET ?)`)
	checkError(err)

	selectTraining, err := db.Preparex(`
		SELECT * FROM training_set WHERE id NOT IN (
		SELECT * FROM (SELECT id FROM training_set WHERE sentiment = 1 ORDER BY id LIMIT ? OFFSET ?) UNION
		SELECT * FROM (SELECT id FROM training_set WHERE sentiment = 0 ORDER BY id LIMIT ? OFFSET ?) UNION
		SELECT * FROM (SELECT id FROM training_set WHERE sentiment = -1 ORDER BY id LIMIT ? OFFSET ?))`)
	checkError(err)

	// Calculate count of data per sentiment
	listCount := []CountSentiment{}
	err = db.Select(&listCount, "SELECT sentiment, COUNT(*) size FROM training_set GROUP BY sentiment")
	checkError(err)

	var (
		nPositive int
		nNeutral  int
		nNegative int
	)

	for _, count := range listCount {
		if count.Sentiment == 1 {
			nPositive = count.Size
		} else if count.Sentiment == 0 {
			nNeutral = count.Size
		} else if count.Sentiment == -1 {
			nNegative = count.Size
		}
	}

	// Calculate limit for each iteration
	limitPositive := math.Floor(float64(nPositive)/float64(k) + 0.5)
	limitNeutral := math.Floor(float64(nNeutral)/float64(k) + 0.5)
	limitNegative := math.Floor(float64(nNegative)/float64(k) + 0.5)

	// Create test result
	testResult := []TestResult{}

	// Create cleaner
	cleaner := NewCleaner()

	// Define bayesian class
	classPositive := bayesian.Class("positive")
	classNegative := bayesian.Class("negative")
	classNeutral := bayesian.Class("neutral")

	// Begin cross validation
	for i := 1; i <= k; i++ {
		// Show message
		fmt.Println("")
		fmt.Println("Iteration", i)

		// Calculate offset
		offsetPositive := int(limitPositive) * (i - 1)
		offsetNeutral := int(limitNeutral) * (i - 1)
		offsetNegative := int(limitNegative) * (i - 1)

		// Create select arguments
		args := []interface{}{
			limitPositive, offsetPositive,
			limitNeutral, offsetNeutral,
			limitNegative, offsetNegative,
		}

		// Fetch testing set
		testingSet := []Tweet{}
		err = selectTesting.Select(&testingSet, args...)
		checkError(err)

		// Fetch training set
		trainingSet := []Tweet{}
		err = selectTraining.Select(&trainingSet, args...)
		checkError(err)

		// Create classifier
		classifier := bayesian.NewClassifier(bayesian.MultinomialBoolean)

		// Create progress bar for training
		progressTraining := pb.StartNew(len(trainingSet)).Prefix("Training")

		// Begin training
		for _, doc := range trainingSet {
			// Clean document
			words := cleaner.Clean(doc.Tweet)

			// If there are no words, skip
			if len(words) == 0 {
				continue
			}

			// Check document class
			var docClass bayesian.Class
			if doc.Sentiment == 1 {
				docClass = classPositive
			} else if doc.Sentiment == 0 {
				docClass = classNeutral
			} else {
				docClass = classNegative
			}

			// Do learning for this doc
			learningDoc := bayesian.NewDocument(docClass, words...)
			classifier.Learn(learningDoc)

			// Increment progress bar
			progressTraining.Increment()
		}

		// Finish progress bar for training
		progressTraining.Finish()

		// Create testing result
		result := TestResult{
			Iteration: i,
		}

		// Create progress bar for testing
		progressTesting := pb.StartNew(len(testingSet)).Prefix("Testing")

		// Begin testing
		for _, doc := range testingSet {
			// Clean document
			words := cleaner.Clean(doc.Tweet)

			// Classify document
			_, class, _ := classifier.Classify(words...)

			// Save result
			if doc.Sentiment == 1 && class == classPositive {
				result.NPositive++
			} else if doc.Sentiment == 0 && class == classNeutral {
				result.NNeutral++
			} else if doc.Sentiment == -1 && class == classNegative {
				result.NNegative++
			}

			// Increment progress bar
			progressTesting.Increment()
		}

		// Finish progress bar for testing
		progressTesting.Finish()

		// Push iteration result to main testing result
		testResult = append(testResult, result)
	}

	// Create table
	fmt.Println("")
	fmt.Println("Final result")
	table := tablewriter.NewWriter(os.Stdout)

	// Write header
	table.SetHeader([]string{
		"Iteration",
		"% Positive",
		"% Negative",
		"% Neutral",
		"% Total",
	})

	// Write content and calculate sum of percentage
	var (
		sumPositive float64
		sumNegative float64
		sumNeutral  float64
		sumTotal    float64
	)

	for _, result := range testResult {
		totalResult := result.NPositive + result.NNegative + result.NNeutral
		totalTestingSize := limitPositive + limitNegative + limitNeutral
		percentPositive := float64(result.NPositive) / limitPositive * 100.0
		percentNegative := float64(result.NNegative) / limitNegative * 100.0
		percentNeutral := float64(result.NNeutral) / limitNeutral * 100.0
		percentTotal := float64(totalResult) / totalTestingSize * 100.0

		table.Append([]string{
			fmt.Sprintf("%d", result.Iteration),
			fmt.Sprintf("%.2f", percentPositive),
			fmt.Sprintf("%.2f", percentNegative),
			fmt.Sprintf("%.2f", percentNeutral),
			fmt.Sprintf("%.2f", percentTotal),
		})

		sumPositive += percentPositive
		sumNegative += percentNegative
		sumNeutral += percentNeutral
		sumTotal += percentTotal
	}

	// Calculate and write average
	avgPositive := sumPositive / float64(k)
	avgNegative := sumNegative / float64(k)
	avgNeutral := sumNeutral / float64(k)
	avgTotal := sumTotal / float64(k)

	table.SetFooter([]string{
		"Average",
		fmt.Sprintf("%.2f", avgPositive),
		fmt.Sprintf("%.2f", avgNegative),
		fmt.Sprintf("%.2f", avgNeutral),
		fmt.Sprintf("%.2f", avgTotal),
	})

	// Render table
	table.Render()
}

func checkError(err error) {
	if err != nil && err != sql.ErrNoRows {
		panic(err)
	}
}
