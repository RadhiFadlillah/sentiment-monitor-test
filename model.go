package main

// Tweet is container for tweet's data.
type Tweet struct {
	ID        int    `db:"id"`
	Tweet     string `db:"tweet"`
	Sentiment int    `db:"sentiment"`
}

// CountSentiment is container for data count of each sentiment
type CountSentiment struct {
	Sentiment int `db:"sentiment"`
	Size      int `db:"size"`
}

// TestResult is container for test result
type TestResult struct {
	Iteration int
	NData     int
	NPositive int
	NNegative int
	NNeutral  int
}
